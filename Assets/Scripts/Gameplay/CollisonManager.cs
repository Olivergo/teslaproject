using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisonManager : MonoBehaviour {

	public GameObject ObstacleManager;
	public GameObject SceneManager;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}




	void OnCollisionEnter2D(Collision2D col)
	{
		if (col.gameObject.tag == "Obstacle") {
			Debug.Log ("Hit an object!");
			SceneManager.GetComponent<GameOver> ().InitGameOver ();
		}
	}
}

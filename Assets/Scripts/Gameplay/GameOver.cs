using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOver : MonoBehaviour {


	public GameObject car;
	public GameObject gameOverScreen;
	public GameObject hud;


	public void InitGameOver(){
		hud.SetActive (false);
        car.GetComponent<CarController>().isDriving = false;
        car.GetComponent<CarController>().speed = 0;
		gameOverScreen.SetActive (true);
        gameOverScreen.GetComponent<GameOverUI>().OnDeath();
	}
}

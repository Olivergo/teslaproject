﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paralax2 : MonoBehaviour {


	public GameObject road1;
	public GameObject road2;
	public GameObject road3;
	public GameObject player;
	public Camera playerCamera;
	public Transform cameraTransform;
	public float viewZone = 10;

	public GameObject lastObject;


	/*
	 * ---Main Purpose---
		The main purpose of this script is allow Gameobjects to change position,
		depending on how far they are away from the player and camera.
	*/

	void Update () {

		//This line takes the location of the camera, and applies it's values to itself.
		cameraTransform = Camera.main.transform;

		//These IF functions are used to check if the GameObject is out of view. The viewzone is defined
		//as a value in the inspector.
		if (cameraTransform.position.y > road1.transform.position.y + viewZone) {
			lastObject = road1;
			Moveroad ();
			Debug.Log ("road is out of veiw!");
		}
		if (cameraTransform.position.y > road2.transform.position.y + viewZone) {
			lastObject = road2;
			Moveroad ();
			Debug.Log ("road is out of veiw!");
		}
		if (cameraTransform.position.y > road3.transform.position.y + viewZone) {
			lastObject = road3;
			Moveroad ();
			Debug.Log ("road is out of veiw!");
		}
	}

	//This function is called once a road is out of view. The function then moves the road defined a set value
	//away from the player's position, which is defined in the New Vector3.
	public void Moveroad(){
		Debug.Log ("Moving road");
		lastObject.transform.position = new Vector3 (0, player.transform.position.y, 0) + new Vector3 (0, 21.0F, 0);
	}

}

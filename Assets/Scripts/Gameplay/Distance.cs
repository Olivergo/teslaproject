﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Distance : MonoBehaviour {

	public float distance;
	public int distanceMultiplier;
	public GameObject car;
	public Text distanceText;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		distanceText.text = Mathf.Round(distance).ToString ();
	}


	void FixedUpdate() {    
		distance += car.GetComponent<CarController> ().speed * distanceMultiplier;
	}
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleManager : MonoBehaviour {

    //Obstancles
    //Car
    float CarSpeed;
	public float spawnRate;
	public int distanceBetweenSpawns;
	public GameObject[] obstacleObjects;
	public Vector2 spawnPoint;
	int randObjectNumber;
	int randLocationNumber;
	public GameObject DistanceManager;
	public GameObject car;


    //Box
    //Tire

	void Start () {
		
	}
	
	void Update () {
		spawnRate = DistanceManager.GetComponent<Distance> ().distance;
		if (spawnRate > distanceBetweenSpawns) {
			SpawnObjects ();
			distanceBetweenSpawns += distanceBetweenSpawns;
		}
	}




	public void SpawnObjects(){
		randObjectNumber = Random.Range (0, 2);
		randLocationNumber = Random.Range (0, 3);
		if (randLocationNumber == 0) {
			spawnPoint.x = -1.218F;
			spawnPoint.y = car.GetComponent<Transform> ().position.y + 20;
		}
		if (randLocationNumber == 1) {
			spawnPoint.x = 0;
			spawnPoint.y = car.GetComponent<Transform> ().position.y + 20;
		}
		if (randLocationNumber == 2) {
			spawnPoint.x = 1.218F;
			spawnPoint.y = car.GetComponent<Transform> ().position.y + 20;
		}

		Instantiate (obstacleObjects [randObjectNumber], spawnPoint, Quaternion.identity);
	}


}

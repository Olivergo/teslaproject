﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    Transform carPos;
     
	// Use this for initialization
	void Start () {
        carPos = GameObject.FindGameObjectWithTag("Car").GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
        this.transform.position = new Vector3(0,carPos.position.y+2,-30);
	}
}

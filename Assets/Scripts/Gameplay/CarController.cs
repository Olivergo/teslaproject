using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarController : MonoBehaviour {
    [Header ("CarSpeed")]
    public float baseSpeed = 0.5f;
    public float speed;
    public bool isDriving;


    // Update is called once per frame
    void Update () {
    if (isDriving == true){
        if ( Input.GetKeyDown(KeyCode.A)) {
            transform.Translate(-1.2f, 0, 0);
        } else if ( Input.GetKeyDown(KeyCode.D)) {
            transform.Translate(1.2f, 0, 0);
        }
    }
        Vector3 clampedPosition = transform.position;
        clampedPosition.x = Mathf.Clamp(transform.position.x, -1.2f, 1.2f);
        transform.position = clampedPosition;

        transform.Translate(0, speed, 0);

    }

    void FixedUpdate() {
        if (isDriving == true){
        speed = baseSpeed += 0.00005f;
        }
    }


}

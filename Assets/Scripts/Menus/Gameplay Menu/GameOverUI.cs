using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverUI : MonoBehaviour {
    
    public int score;
    public int hiScore;
    public int nextRoad;
    public Text scoreText;
    public Text hiScoreText;
    public Text nextRoadText;
   GameObject playerSettings;
    
    public GameObject distanceManager;
    
    void Start(){
        
    }
    
    public void OnDeath()
    {
        playerSettings = GameObject.FindGameObjectWithTag("PlayerSettings");
        score = Mathf.RoundToInt(distanceManager.GetComponent<Distance>().distance);
        nextRoad = playerSettings.GetComponent<PlayerSettings>().level2Distance - score;
        if (score > hiScore){
            hiScore = score;
        }
        scoreText.text = score.ToString();
        hiScoreText.text = hiScore.ToString();
        nextRoadText.text = nextRoad.ToString();
        Save();
        
    }
    
    public void Save(){
        playerSettings.GetComponent<PlayerSettings>().lvl1HiScore = hiScore;
        playerSettings.GetComponent<PlayerSettings>().totalDistance += score;
        playerSettings.GetComponent<PlayerSettings>().SavePlayerProgression();
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneChange : MonoBehaviour {

	public GameObject fadeObject;
	public float fadingSpeed;
	public bool isFadingOut;
	public bool isFadingIn;
	public string sceneToLoad;



	/*
	 * ---Main Purpose---
		The main purpose of this script is change scenes, with a screen fade to make	  
		the transsition smoother.
	*/

	void Start () {
		//At the start of every scene start, this function activates the fade out process.
		//Every scene always starts with a black screen.
		FadeOut ();
	}
	

	void Update () {
		//These IF statements calls the Animator, that is placed on the fade object defined above.
		if (isFadingOut == true) {
			
		}
		if (isFadingIn == true) {
			
		}
	}
	public void FadeOut(){
		isFadingOut = true;
		fadeObject.GetComponent<Animator> ().SetBool ("IsFadingOut", true);
		StartCoroutine (FadeWait ());
	}
	public void FadeIn(){
		//It is important to deactivate the fade object, as players will not be able to click on the screen
		//otherwise.
		fadeObject.SetActive (true);
		isFadingIn = true;
		fadeObject.GetComponent<Animator> ().SetBool ("IsFadingIn", true);
		StartCoroutine (SceneFadeWait ());
	}

	IEnumerator SceneFadeWait(){
		yield return new WaitForSeconds (2);
		SceneManager.LoadScene (sceneToLoad);
	}
	IEnumerator FadeWait(){
		yield return new WaitForSeconds (2);
		isFadingOut = false;
	
		fadeObject.SetActive (false);
	}

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class MasterAudio : MonoBehaviour {

	public AudioSource music;
	public AudioSource sound;
	public Slider musicSlider;
	public Slider soundSlider;
	public float musicVolume;
	public float soundVolume;
	public GameObject playerSettings;

	/*
	 	 ---Main Purpose---
		This script is used to control all audio throughout the application. 
		The script saves music and sound options to the disk. So the data can be used in other scenes
		and across sessions. 
	*/

	void Start(){
		//This line makes the AudioSource ignore options set by the sound setting. 
		//Used to divide the options.
		music.ignoreListenerVolume = true;
	}

	void Update(){

		AudioListener.volume = soundSlider.value;
		music.volume = musicSlider.value;
		sound.volume = soundSlider.value;
	}

	//This function saves and moves the data to PlayerSettings, that handles all data that needs saving to
	//disk.
	public void SaveSettings(){
		musicVolume = music.volume;
		soundVolume = sound.volume;
		playerSettings.GetComponent<PlayerSettings> ().musicVolume = musicVolume;
		playerSettings.GetComponent<PlayerSettings> ().soundVolume = soundVolume;
		playerSettings.GetComponent<PlayerSettings> ().SaveOptions ();
	}
}

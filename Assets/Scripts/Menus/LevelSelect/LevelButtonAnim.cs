﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelButtonAnim : MonoBehaviour {


	public GameObject levelInfo;
	public bool isActive;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}



	public void LevelInfoActivate(){
		if (isActive == false) {
			levelInfo.SetActive (true);
			levelInfo.GetComponent<Animator> ().SetBool ("IsFadingIn", true);
			isActive = true;
			return;
		}
		if (isActive == true) {
			levelInfo.GetComponent<Animator> ().SetBool ("IsFadingIn", false);
			levelInfo.GetComponent<Animator> ().SetBool ("IsFadingOut", true);
			isActive = false;
			StartCoroutine (DeactivateObject ());
			return;
		}
	}
	IEnumerator DeactivateObject(){
		yield return new WaitForSeconds (1);
		levelInfo.SetActive (false);
	}

	public void OnPointerEnter(){
		gameObject.GetComponent<Animator> ().SetBool ("IsMouseOver", true);
	}
	public void OnPointerExit(){
		gameObject.GetComponent<Animator> ().SetBool ("IsMouseOver", false);
	}

}

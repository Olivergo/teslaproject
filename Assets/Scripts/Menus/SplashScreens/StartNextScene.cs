﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartNextScene : MonoBehaviour {

	public int waitTime;

	// Use this for initialization
	void Start () {
		StartCoroutine (ChangeScene ());
	}
	
	IEnumerator ChangeScene (){
		yield return new WaitForSeconds (waitTime);
		gameObject.GetComponent<SceneChange> ().FadeIn();
	}
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSettings : MonoBehaviour {

	public float musicVolume;
	public float soundVolume;
    public int lvl1HiScore;
    public int lvl2HiScore;
    public int totalDistance;
    public int level2Distance;
    public int level3Distance;
    

	/*
	 * ---Main Purpose---
		This script is used to hold data that is used throughout the application.
	*/

	void Start () {
		DontDestroyOnLoad (this);
	}
	

	void Update () {
		
	}


	public void SaveOptions(){
		PlayerPrefs.SetFloat ("Music Volume",musicVolume);
		PlayerPrefs.SetFloat ("Sound Volume",soundVolume);
        
	}
	public void SavePlayerProgression(){
        PlayerPrefs.SetInt("Level 1 HighScore", lvl1HiScore);
        PlayerPrefs.SetInt("Total Distance", totalDistance);
	}
}

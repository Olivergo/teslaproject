﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ChangeMusicVolume : MonoBehaviour {

	public Slider Volume;
	public AudioSource myMusic;

	void Update(){
		myMusic.volume = Volume.value;
	}
}
